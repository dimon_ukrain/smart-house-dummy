-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Чрв 19 2020 р., 05:50
-- Версія сервера: 10.3.13-MariaDB
-- Версія PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `smart-house`
--

-- --------------------------------------------------------

--
-- Структура таблиці `arduino`
--

CREATE TABLE `arduino` (
  `id` int(11) NOT NULL,
  `house_room_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `arduino`
--

INSERT INTO `arduino` (`id`, `house_room_id`, `token`) VALUES
(1, 1, 'efe69cdda3d4775c94bddc815ae281a0');

-- --------------------------------------------------------

--
-- Структура таблиці `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `control_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `devices`
--

INSERT INTO `devices` (`id`, `name`, `short_description`, `description`, `control_type`) VALUES
(1, 'Сетевой модуль', 'W5500', NULL, NULL),
(3, 'Датчик влажности', 'DHT22', 'При проектировании домашних оранжерей и метеостанций, теплиц и инкубаторов, систем умного дома стоит купить датчик влажности и температуры DHT22 и реализовать его возможности в своем проекте. Он предназначен для работы на базе микросхем Arduino, для подключения к которым имеется три вывода. Датчик температуры DHT22 имеет низкое энергопотребление, продолжительный срок эксплуатации и не требует дополнительной обвязки. Способен эффективно работать при подключении с помощью длинного провода. Датчик DHT22 имеет широкий рабочий диапазон и высокую точность измерений, отличается простотой программирования. Использование этого сенсора позволит реализовать такие проекты, как автоматизация увлажнения и охлаждения помещения. Датчик поможет подать сигнал тревоги при достижении критических показателей температуры или влажности на объекте. Передача данных осуществляется с помощью цифрового сигнала, что позволяет транслировать его по кабелю на несколько десятков метров.', 'display_info'),
(4, 'Часы реального времени', 'DS3231', 'Если вы создаете устройство, которому нужно знать точное время, вам пригодится модуль часов реального времени RTC (Real Time Clock). Данные модули отсчитывают точное время и могут сохранять его даже при отключении основного питания при использовании резервного питания (батарейка CR2032 или литий-ионный аккумулятор LIR2032-3,6 В), которого хватит на несколько лет.\r\n\r\nЕще совсем недавно основным модулем RTC в среде Ардуинщиков являлся модуль на микросхеме DS1307. В этом модуле использовался внешний кварцевый генератор частотой 32кГц, при изменении температуры менялась частота кварца, что приводило к погрешности в подсчете времени.\r\n\r\nНовые модули RTC (рис. 1) построены на микросхеме DS3231, внутрь которой установлен кварцевый генератор и датчик температуры, который компенсирует изменения температуры, поэтому время отсчитывается более точно. Погрешность составляет ±2 минуты за год. ', 'display_info'),
(5, 'Датчик газа', 'MQ-135', 'Датчик газа MQ-135 - датчик контроля качества воздуха, поможет вам в обнаружении вредных веществ в окружающей среде (углекислый газ, угарный газ, аммиак, бензол, оксид азота и пары спирта). Напряжение на выходе модуля повышается с увеличением концентрации газа. Модуль имеет быстрый отклик и малое время восстановления. Для использования в различных условиях применения MQ-135 имеет возможность подстройки чувствительности.\r\n (аммиак, бензол, спирт, дым)', 'display_info'),
(6, 'Датчик воды', 'water sensor', NULL, 'button'),
(9, 'Инфракрасный датчик движения', 'HC-SR501', 'Инфракрасные датчики движения HC-SR501 - позволяют определять движения людей, животных или других объектов , которые излучают тепло. Данные датчики не дорогие, компактные, с низким энергопотреблением, простые в использовании и долговечные. По этим причинам они часто встречаются в различных устройствах и проектах. На выходе сенсора, пока движения нет, выходной сигнал - логический ноль. При возникновении движения, сигнальный контакт устанавливается в логическую единицу на короткий промежуток времени. HC-SR501 может работать в двух режимах : режим H и режим L. Режим H — в этом режиме при срабатывании датчика несколько раз подряд на его выходе (на OUT) остается высокий логический уровень. Режим L — в этом режиме на выходе при каждом срабатывании датчика появляется отдельный импульс. Режим работы модуля задается перемычкой.', 'switch-on'),
(10, 'Датчик атмосферного давления', 'BME280', 'Высокоточный датчика атмосферного давления, температуры и влажности BME280 часто используется в Arduino проектах. Управление модулем датчика возможно как по I2C интерфейсу, так и по SPI. По сравнению с первыми датчиками серии (BMP085 и BMP180) он имеет лучшие характеристики и меньшие размеры. Отличие от датчика BMP280 – наличие гигрометра, который позволяет измерять относительную влажность воздуха.\r\nБлагодаря высокой точности и большим диапазонам измерения необходимых показателей, BME280 является идеальным решением для климатических проектов, в частности для создания метеостанций. ', 'display_info'),
(13, 'Реле', 'реле', 'Реле', 'switch-on'),
(14, 'Led лента', 'Led лента', 'Led лента', 'switch-on'),
(15, 'Датчик температуры', 'Датчик температуры', 'Датчик температуры', 'display_info'),
(16, 'Сервопривод', 'Сервопривод', 'Сервопривод', 'display_info'),
(17, 'Вентилятор', 'Вентилятор', 'Вентилятор', 'switch-on'),
(23, 'Датчик температури', 'DHT22', '', 'display_info'),
(24, 'Датчик температури', 'BME280', '', 'display_info'),
(25, 'Датчик влажности', 'BME280', '', 'display_info');

-- --------------------------------------------------------

--
-- Структура таблиці `houses`
--

CREATE TABLE `houses` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `houses`
--

INSERT INTO `houses` (`id`, `owner_id`, `description`, `room_amount`) VALUES
(1, 18, 'Дом 1. кв. 1', 2),
(2, 19, 'Дом 2. кв. 1', 3),
(3, 18, 'Дом 1 кв. 2', 2),
(4, 20, 'Великий дом, Великого Жени', 3),
(12, 18, 'CreatorHouse', 3),
(15, 18, 'test2', 3);

-- --------------------------------------------------------

--
-- Структура таблиці `house_rooms`
--

CREATE TABLE `house_rooms` (
  `id` int(11) NOT NULL,
  `house_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `house_rooms`
--

INSERT INTO `house_rooms` (`id`, `house_id`, `room_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 3, 5),
(5, 3, 4),
(6, 3, 6),
(7, 4, 6),
(8, 1, 3),
(9, 1, 4),
(10, 1, 5),
(11, 1, 6),
(16, 15, 2),
(17, 15, 3),
(19, 15, 1);

-- --------------------------------------------------------

--
-- Структура таблиці `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_src` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `img_src`, `description`) VALUES
(1, 'Living-room', 'living_room.png', NULL),
(2, 'Bathroom', 'bathroom.png', NULL),
(3, 'Bedroom', 'bedroom.png', NULL),
(4, 'Kitchen', 'kitchen.png', NULL),
(5, 'Washing room', 'washing-room.png', NULL),
(6, 'Family-room', 'family-room.png', NULL);

-- --------------------------------------------------------

--
-- Структура таблиці `room_devices`
--

CREATE TABLE `room_devices` (
  `id` int(11) NOT NULL,
  `house_rooms_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `device_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `room_devices`
--

INSERT INTO `room_devices` (`id`, `house_rooms_id`, `device_id`, `device_value`) VALUES
(1, 1, 10, '740.24'),
(2, 1, 3, '67.30'),
(3, 1, 5, '0.00'),
(4, 1, 6, 'off'),
(7, 3, 3, '0'),
(8, 3, 4, '0'),
(9, 1, 9, 'off'),
(10, 7, 5, '50'),
(11, 7, 10, '150'),
(12, 7, 6, 'on'),
(13, 7, 9, 'on'),
(15, 19, 17, 'on'),
(16, 19, 16, '32'),
(20, 1, 23, '26.70'),
(21, 1, 24, '25.88'),
(22, 1, 25, '56.49'),
(23, 1, 13, 'off'),
(24, 1, 14, 'off'),
(25, 1, 17, 'on'),
(26, 1, 16, '0'),
(27, 1, 16, '0');

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `register_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `birthday_date` date DEFAULT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `gender`, `picture_img`, `register_date`, `birthday_date`, `role`) VALUES
(18, 'Администратор', '$2y$10$wYg1YFnNaZWudKFvQjYX/.ELbDYzhSF9i3Gn/15gwb7eQ.yaf8jl.', 'Osip.29.1999@gmail.com', NULL, NULL, '2020-05-05 11:44:15', NULL, 'admin'),
(19, 'Дима Осипчук', '$2y$10$ARRR8LXlJ8lC79xRGflKe.FH5eII69VfP1gfCOHZNJ7IqI/zbGB6y', 'dimon_ukrain@mail.ru', NULL, NULL, '2020-05-05 11:44:27', NULL, NULL),
(20, 'Петричук Євген Володимирович', '$2y$10$R4SHQSGO5SnLWNxwdxsMgeD28tBssAMB8H7pAmm3Wem8apYI9GRA2', 'petrychukyegen@gmail.com', NULL, NULL, '2020-05-23 09:31:12', NULL, 'admin');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `arduino`
--
ALTER TABLE `arduino`
  ADD PRIMARY KEY (`id`),
  ADD KEY `	house_room_id` (`house_room_id`);

--
-- Індекси таблиці `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `houses`
--
ALTER TABLE `houses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Owner_id` (`owner_id`);

--
-- Індекси таблиці `house_rooms`
--
ALTER TABLE `house_rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `House_id` (`house_id`),
  ADD KEY `Room_id` (`room_id`);

--
-- Індекси таблиці `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `room_devices`
--
ALTER TABLE `room_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `device-id` (`device_id`),
  ADD KEY `house_rooms_id` (`house_rooms_id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `arduino`
--
ALTER TABLE `arduino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблиці `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблиці `houses`
--
ALTER TABLE `houses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблиці `house_rooms`
--
ALTER TABLE `house_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблиці `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблиці `room_devices`
--
ALTER TABLE `room_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `arduino`
--
ALTER TABLE `arduino`
  ADD CONSTRAINT `	house_room_id` FOREIGN KEY (`house_room_id`) REFERENCES `house_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `houses`
--
ALTER TABLE `houses`
  ADD CONSTRAINT `Owner_id` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`);

--
-- Обмеження зовнішнього ключа таблиці `house_rooms`
--
ALTER TABLE `house_rooms`
  ADD CONSTRAINT `House_id` FOREIGN KEY (`house_id`) REFERENCES `houses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Room_id` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `room_devices`
--
ALTER TABLE `room_devices`
  ADD CONSTRAINT `device-id` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `house_rooms_id` FOREIGN KEY (`house_rooms_id`) REFERENCES `house_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
