-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 22 2020 г., 20:01
-- Версия сервера: 5.7.25
-- Версия PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `smart-house`
--

-- --------------------------------------------------------

--
-- Структура таблицы `arduino`
--

CREATE TABLE `arduino` (
  `id` int(11) NOT NULL,
  `house_room_id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `arduino`
--

INSERT INTO `arduino` (`id`, `house_room_id`, `token`) VALUES
(1, 1, 'efe69cdda3d4775c94bddc815ae281a0'),
(3, 2, 'q'),
(4, 2, 'qwerty');

-- --------------------------------------------------------

--
-- Структура таблицы `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `output_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `devices`
--

INSERT INTO `devices` (`id`, `name`, `short_description`, `description`, `output_type_id`) VALUES
(1, 'Датчик влажности', 'DHT22', 'При проектировании домашних оранжерей и метеостанций, теплиц и инкубаторов, систем умного дома стоит купить датчик влажности и температуры DHT22 и реализовать его возможности в своем проекте. Он предназначен для работы на базе микросхем Arduino, для подключения к которым имеется три вывода. Датчик температуры DHT22 имеет низкое энергопотребление, продолжительный срок эксплуатации и не требует дополнительной обвязки. Способен эффективно работать при подключении с помощью длинного провода. Датчик DHT22 имеет широкий рабочий диапазон и высокую точность измерений, отличается простотой программирования. Использование этого сенсора позволит реализовать такие проекты, как автоматизация увлажнения и охлаждения помещения. Датчик поможет подать сигнал тревоги при достижении критических показателей температуры или влажности на объекте. Передача данных осуществляется с помощью цифрового сигнала, что позволяет транслировать его по кабелю на несколько десятков метров.', 1),
(2, 'Часы реального времени', 'DS3231', 'Если вы создаете устройство, которому нужно знать точное время, вам пригодится модуль часов реального времени RTC (Real Time Clock). Данные модули отсчитывают точное время и могут сохранять его даже при отключении основного питания при использовании резервного питания (батарейка CR2032 или литий-ионный аккумулятор LIR2032-3,6 В), которого хватит на несколько лет.\r\n\r\nЕще совсем недавно основным модулем RTC в среде Ардуинщиков являлся модуль на микросхеме DS1307. В этом модуле использовался внешний кварцевый генератор частотой 32кГц, при изменении температуры менялась частота кварца, что приводило к погрешности в подсчете времени.\r\n\r\nНовые модули RTC (рис. 1) построены на микросхеме DS3231, внутрь которой установлен кварцевый генератор и датчик температуры, который компенсирует изменения температуры, поэтому время отсчитывается более точно. Погрешность составляет ±2 минуты за год. ', 1),
(3, 'Часы реального времени', 'DS3231', 'Если вы создаете устройство, которому нужно знать точное время, вам пригодится модуль часов реального времени RTC (Real Time Clock). Данные модули отсчитывают точное время и могут сохранять его даже при отключении основного питания при использовании резервного питания (батарейка CR2032 или литий-ионный аккумулятор LIR2032-3,6 В), которого хватит на несколько лет.\r\n\r\nЕще совсем недавно основным модулем RTC в среде Ардуинщиков являлся модуль на микросхеме DS1307. В этом модуле использовался внешний кварцевый генератор частотой 32кГц, при изменении температуры менялась частота кварца, что приводило к погрешности в подсчете времени.\r\n\r\nНовые модули RTC (рис. 1) построены на микросхеме DS3231, внутрь которой установлен кварцевый генератор и датчик температуры, который компенсирует изменения температуры, поэтому время отсчитывается более точно. Погрешность составляет ±2 минуты за год. ', 1),
(4, 'Датчик газа', 'MQ-135', 'Датчик газа MQ-135 - датчик контроля качества воздуха, поможет вам в обнаружении вредных веществ в окружающей среде (углекислый газ, угарный газ, аммиак, бензол, оксид азота и пары спирта). Напряжение на выходе модуля повышается с увеличением концентрации газа. Модуль имеет быстрый отклик и малое время восстановления. Для использования в различных условиях применения MQ-135 имеет возможность подстройки чувствительности.\r\n (аммиак, бензол, спирт, дым)', 1),
(5, 'Датчик воды', 'Датчик воды', '', 2),
(6, 'Инфракрасный датчик движения', 'HC-SR501', 'Инфракрасные датчики движения HC-SR501 - позволяют определять движения людей, животных или других объектов , которые излучают тепло. Данные датчики не дорогие, компактные, с низким энергопотреблением, простые в использовании и долговечные. По этим причинам они часто встречаются в различных устройствах и проектах. На выходе сенсора, пока движения нет, выходной сигнал - логический ноль. При возникновении движения, сигнальный контакт устанавливается в логическую единицу на короткий промежуток времени. HC-SR501 может работать в двух режимах : режим H и режим L. Режим H — в этом режиме при срабатывании датчика несколько раз подряд на его выходе (на OUT) остается высокий логический уровень. Режим L — в этом режиме на выходе при каждом срабатывании датчика появляется отдельный импульс. Режим работы модуля задается перемычкой.', 3),
(7, 'Датчик атмосферного давления', 'BME280', 'Высокоточный датчика атмосферного давления, температуры и влажности BME280 часто используется в Arduino проектах. Управление модулем датчика возможно как по I2C интерфейсу, так и по SPI. По сравнению с первыми датчиками серии (BMP085 и BMP180) он имеет лучшие характеристики и меньшие размеры. Отличие от датчика BMP280 – наличие гигрометра, который позволяет измерять относительную влажность воздуха.\r\nБлагодаря высокой точности и большим диапазонам измерения необходимых показателей, BME280 является идеальным решением для климатических проектов, в частности для создания метеостанций. ', 1),
(8, 'Реле', 'Реле', 'Реле', 3),
(9, 'Led лента', 'Led лента', 'Led лента', 3),
(10, 'Датчик температуры', 'Датчик температуры', 'Датчик температуры', 1),
(11, 'Сервопривод', 'Сервопривод', 'Сервопривод', 1),
(12, 'Вентилятор', 'Вентилятор', 'Вентилятор', 3),
(13, 'Датчик температури', 'Датчик температури', 'Датчик температури', 1),
(14, 'Датчик влажности', 'Датчик влажности', 'Датчик влажности', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `houses`
--

CREATE TABLE `houses` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `room_amount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `houses`
--

INSERT INTO `houses` (`id`, `owner_id`, `description`, `room_amount`) VALUES
(1, 18, 'Будинок 15. кв. 24', 2),
(2, 19, 'Будинок 2. кв. 1', 3),
(3, 18, 'Будинок 3 кв. 2', 2),
(4, 20, 'Великий Будинок, Великого Жени', 3),
(12, 18, 'Будинок 15, кв. 66', 3),
(15, 18, 'Будинок тестувальника', 3),
(16, 18, 'Будинок тестувальника 2', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `house_rooms`
--

CREATE TABLE `house_rooms` (
  `id` int(11) NOT NULL,
  `house_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `house_rooms`
--

INSERT INTO `house_rooms` (`id`, `house_id`, `room_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(4, 3, 5),
(5, 3, 4),
(6, 3, 6),
(7, 4, 6),
(8, 1, 3),
(9, 1, 4),
(10, 1, 5),
(11, 1, 6),
(16, 15, 2),
(17, 15, 3),
(19, 15, 1),
(20, 16, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `output_types`
--

CREATE TABLE `output_types` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_src` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `output_types`
--

INSERT INTO `output_types` (`id`, `type`, `img_src`) VALUES
(1, 'display_info', 'display_info.png'),
(2, 'button', 'button.png'),
(3, 'switch-on', 'switch-on.png');

-- --------------------------------------------------------

--
-- Структура таблицы `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_src` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `img_src`, `description`) VALUES
(1, 'Вітальня кімната', 'living_room.png', NULL),
(2, 'Ванна кімната', 'bathroom.png', NULL),
(3, 'Спальня кімната', 'bedroom.png', NULL),
(4, 'Кухня', 'kitchen.png', NULL),
(5, 'Пральня', 'washing-room.png', NULL),
(6, 'Сімейна кімната', 'family-room.png', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `room_devices`
--

CREATE TABLE `room_devices` (
  `id` int(11) NOT NULL,
  `house_rooms_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `device_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `room_devices`
--

INSERT INTO `room_devices` (`id`, `house_rooms_id`, `device_id`, `device_value`) VALUES
(1, 1, 1, '100'),
(2, 1, 7, '740.24'),
(3, 1, 1, '67.30'),
(4, 1, 4, '0.00'),
(5, 1, 5, 'on'),
(7, 1, 6, 'on'),
(8, 1, 13, '100'),
(9, 1, 12, 'on');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `birthday_date` date DEFAULT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `gender`, `picture_img`, `register_date`, `birthday_date`, `role`) VALUES
(18, 'Администратор', '$2y$10$wYg1YFnNaZWudKFvQjYX/.ELbDYzhSF9i3Gn/15gwb7eQ.yaf8jl.', 'Osip.29.1999@gmail.com', NULL, NULL, '2020-05-05 11:44:15', NULL, 'admin'),
(19, 'Дима Осипчук', '$2y$10$ARRR8LXlJ8lC79xRGflKe.FH5eII69VfP1gfCOHZNJ7IqI/zbGB6y', 'dimon_ukrain@mail.ru', NULL, NULL, '2020-05-05 11:44:27', NULL, NULL),
(20, 'Петричук Євген Володимирович', '$2y$10$R4SHQSGO5SnLWNxwdxsMgeD28tBssAMB8H7pAmm3Wem8apYI9GRA2', 'petrychukyegen@gmail.com', NULL, NULL, '2020-05-23 09:31:12', NULL, 'admin');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `arduino`
--
ALTER TABLE `arduino`
  ADD PRIMARY KEY (`id`),
  ADD KEY `	house_room_id` (`house_room_id`);

--
-- Индексы таблицы `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `output_type_id` (`output_type_id`);

--
-- Индексы таблицы `houses`
--
ALTER TABLE `houses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Owner_id` (`owner_id`);

--
-- Индексы таблицы `house_rooms`
--
ALTER TABLE `house_rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `House_id` (`house_id`),
  ADD KEY `Room_id` (`room_id`);

--
-- Индексы таблицы `output_types`
--
ALTER TABLE `output_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `room_devices`
--
ALTER TABLE `room_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `device-id` (`device_id`),
  ADD KEY `house_rooms_id` (`house_rooms_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `arduino`
--
ALTER TABLE `arduino`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `houses`
--
ALTER TABLE `houses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `house_rooms`
--
ALTER TABLE `house_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `output_types`
--
ALTER TABLE `output_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `room_devices`
--
ALTER TABLE `room_devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `arduino`
--
ALTER TABLE `arduino`
  ADD CONSTRAINT `	house_room_id` FOREIGN KEY (`house_room_id`) REFERENCES `house_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `output_type_id` FOREIGN KEY (`output_type_id`) REFERENCES `output_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `houses`
--
ALTER TABLE `houses`
  ADD CONSTRAINT `Owner_id` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `house_rooms`
--
ALTER TABLE `house_rooms`
  ADD CONSTRAINT `House_id` FOREIGN KEY (`house_id`) REFERENCES `houses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Room_id` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `room_devices`
--
ALTER TABLE `room_devices`
  ADD CONSTRAINT `device-id` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `house_rooms_id` FOREIGN KEY (`house_rooms_id`) REFERENCES `house_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
