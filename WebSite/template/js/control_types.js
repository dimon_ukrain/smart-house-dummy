function controlButtonClicked(button) {
    if (button.className === "button_off") {
        button.innerHTML = "ON";
        button.className = "button_on";
    } else {
        button.innerHTML = "OFF";
        button.className = "button_off";
    }
}

function controlSwitchOnClicked(element) {
    element.classList.toggle("switch-on");
}

function turnOffAllDevices() {
    let switchers = document.querySelectorAll(".switch-on");
    let buttons = document.querySelectorAll(".button_on");

    [].forEach.call(switchers, function (el) {
        el.classList.remove("switch-on");
    });

    [].forEach.call(buttons, function (el) {
        el.innerHTML = "OFF";
        el.className = "button_off";
    });
}