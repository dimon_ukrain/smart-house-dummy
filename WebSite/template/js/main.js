document.addEventListener("DOMContentLoaded", () => {
    const houses = document.getElementById("houses");

    if (houses) {
        houses.addEventListener('click', (e)=>{
            if(e.target.className === 'btn btn-danger delete-article') {
                if(confirm('Are you sure?')) {
                    const id = e.target.getAttribute('data-id');
                    fetch(`/house/delete/${id}`, {
                        method: 'POST'
                    }).then(res => window.location.reload())
                } else {
                    e.preventDefault();
                }
            }

        });
    }

    const rooms = document.getElementById("rooms");

    if (rooms) {
        rooms.addEventListener('click', (e)=>{
            if(e.target.className === 'btn btn-danger delete-article') {
                if(confirm('Are you sure?')) {
                    const id = e.target.getAttribute('data-id');
                    fetch(`/room/delete/${id}`, {
                        method: 'POST'
                    }).then(res => window.location.reload())
                } else {
                    e.preventDefault();
                }
            }

        });
    }

    const devices = document.getElementById("devices");
    if (devices) {
        deleteButton(devices, "device");

    }

    const houseRooms = document.getElementById("houseRooms");
    if (houseRooms) {
        deleteButton(houseRooms, "houseRoom");
    }


    const arduinos = document.getElementById("arduinos");
    if (arduinos) {
        deleteButton(arduinos, "arduino");
    }

    const roomDevices = document.getElementById("roomDevices");
    if (roomDevices) {
        deleteButton(roomDevices, "roomDevice");
    }

});

function deleteButton(table, deletedObject) {
    table.addEventListener('click', (e)=>{
        if(e.target.className === 'btn btn-danger delete-article') {

            if(confirm('Are you sure?')) {
                const id = e.target.getAttribute('data-id');
                fetch(`/${deletedObject}/delete/${id}`, {
                    method: 'POST'
                }).then(res => window.location.reload())
            } else {
                e.preventDefault();
            }
        }

    });
}

function modal(){
    let modal = document.getElementById("myModal");
    modal.style.display = "block";

    let span = document.getElementsByClassName("close")[0];

    span.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}


function changeTypeImg() {
    let selectValue = $("#output_type_id option:selected").text() + ".png";
    let imgSrc = "../../../template/img/output-types/" + selectValue;
    $("#output-type-img").attr('src', imgSrc);
}
