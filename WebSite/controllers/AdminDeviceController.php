<?php

include_once 'models/User.php';
include_once 'models/Room.php';
include_once 'models/House.php';
include_once 'models/Device.php';
include_once 'models/Room_devices.php';
include_once 'models/Output_type.php';


class AdminDeviceController
{
    public function actionIndex()
    {
        if (User::isCurrentUserAdmin()) {
            $columns = Device::getColumns();
            $devices = Device::getDevices();

            require_once(ROOT . '/views/site/admin-device/list.php');
        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionCreate()
    {
        if (User::isCurrentUserAdmin()) {
            $columns = Device::getColumns();

            $createdObject = 'device';

            if(!empty($_POST['device'])) {
                $insertAttributes = $_POST['device'];

                Device::create($insertAttributes);
            }

            require_once(ROOT . '/views/site/admin-device/create.php');


        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionEdit($id)
    {
        if (User::isCurrentUserAdmin()) {

            $columns = Device::getColumns();
            $device = Device::getDeviceById($id);

            if(!empty($_POST['device'])) {
                $insertAttributes = $_POST['device'];

                Device::edit($id, $insertAttributes);
            }

            require_once(ROOT . '/views/site/admin-device/edit.php');


        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionDelete($id)
    {
        if (User::isCurrentUserAdmin()) {
            Room_devices::deleteDevices($id);

            Device::delete($id);
        } else {
            echo 'You are not admin';
        }

        return true;
    }

}