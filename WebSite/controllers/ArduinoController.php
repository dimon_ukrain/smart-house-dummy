<?php

include_once 'models/User.php';
include_once 'models/House.php';
include_once 'models/Room_devices.php';
include_once 'models/Room.php';
include_once 'models/Device.php';
include_once 'models/Arduino.php';
include_once 'models/House_rooms.php';

/**
 * Class AdminController
 */
class ArduinoController
{

    public function actionUpdateDevices()
    {
        if(isset($_POST['houseRoomId']) && isset($_POST['token'])) {

            $arduino = Arduino::getArduinoByHouseRoomId($_POST['houseRoomId']);

            if($arduino['token'] == $_POST['token']) {
                $houseRoomId = $_POST['houseRoomId'];
                $roomDevices = Room_devices::getRoomDevicesByHouseRoomsId($houseRoomId);

                if(isset($_POST['arduinoValues'])) {
                    $receivedValue = $_POST['arduinoValues'];

                    foreach ($roomDevices as $roomDevice) {
                        if ($_POST['arduinoValues']['id'] == $roomDevice['id']) {
                            Room_devices::updateDeviceValue( $receivedValue['id'], $receivedValue['value']);
                        }
                    }
                }
            }
        }

        require_once(ROOT . '/views/site/arduino/arduino-response.php');

        return true;
    }
    public function actionArduinoSendResponse()
    {
        if(isset($_GET['houseRoomId']) && isset($_GET['token'])) {

            $arduino = Arduino::getArduinoByHouseRoomId($_GET['houseRoomId']);

            if($arduino['token'] == $_GET['token']) {
                $houseRoomId = $_GET['houseRoomId'];
                $roomDevices = Room_devices::getRoomDevicesByHouseRoomsId($houseRoomId);

                foreach($roomDevices as $roomDevice) {
                    if(isset($_GET[$roomDevice['id']])) {
                        Room_devices::updateDeviceValue($roomDevice['id'], $_GET[$roomDevice['id']]);
                    }
                }
            }
        }

        require_once(ROOT . '/views/site/arduino/arduino-response.php');

        return true;
    }

    //token=efe69cdda3d4775c94bddc815ae281a0&houseRoomId=1&10=27.10&11=66.80&5=397.00&12=25.94&13=56.66&14=739.16
    public function actionArduinoSendRequest()
    {
        if(isset($_GET['houseRoomId'])  && isset($_GET['token'])) {
            $arduino = Arduino::getArduinoByHouseRoomId($_GET['houseRoomId']);
            if($arduino['token'] == $_GET['token']) {
                $devices = Room_devices::getRoomDevicesByHouseRoomsId($_GET['houseRoomId']);
                echo '#';
                foreach ($devices as $device) {
                    echo "{$device['id']}={$device['device_value']};";
                }
            }
        }

        require_once(ROOT . '/views/site/arduino/arduino-request.php');

        return true;
    }

}