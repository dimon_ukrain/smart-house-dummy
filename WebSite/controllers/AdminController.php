<?php

include_once 'models/User.php';
include_once 'models/House.php';
include_once 'models/Room_devices.php';
include_once 'models/Room.php';
include_once 'models/Device.php';
include_once 'models/House_rooms.php';

/**
 * Class AdminController
 */
class AdminController
{
    /**
     * @return bool|string
     */
    public function actionIndex()
    {
        if (User::isAdmin($_SESSION['user']['id'])) {
            require_once(ROOT . '/views/site/admin/index.php');
        } else {
            return 'You are not admin';
        }

        return true;
    }

}