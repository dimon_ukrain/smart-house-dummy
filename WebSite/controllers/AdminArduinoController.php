<?php

include_once 'models/User.php';
include_once 'models/Room.php';
include_once 'models/House.php';
include_once 'models/Device.php';
include_once 'models/Arduino.php';
include_once 'models/Room_devices.php';


class AdminArduinoController
{
    public function actionIndex()
    {
        if (User::isCurrentUserAdmin()) {
            $columns = Arduino::getColumns();
            $arduinos = Arduino::getArduinos();

            require_once(ROOT . '/views/site/admin-arduino/list.php');
        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionCreate()
    {
        if (User::isCurrentUserAdmin()) {
            $columns = Arduino::getColumns();
            $createdObject = 'arduino';

            if(!empty($_POST['arduino'])) {
                $insertAttributes = $_POST['arduino'];

                Arduino::create($insertAttributes);
            }

            require_once(ROOT . '/views/site/admin-arduino/create.php');


        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionEdit($id)
    {
        if (User::isCurrentUserAdmin()) {
            $columns = Arduino::getColumns();
            $arduino = Arduino::getArduinoById($id);

            if(!empty($_POST['arduino'])) {
                $insertAttributes = $_POST['arduino'];

                Arduino::edit($id, $insertAttributes);
            }

            require_once(ROOT . '/views/site/admin-arduino/edit.php');
        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionDelete($id)
    {
        if (User::isCurrentUserAdmin()) {
            Arduino::delete($id);
        } else {
            echo 'You are not admin';
        }

        return true;
    }

}