<?php

include_once 'models/User.php';

class UserController
{
    public function actionLogOut() {
        if (!User::isGuest()) {

            session_destroy();
            header("Location: /login");
        }

        return true;
    }

    public function actionLogin() {
        if(!empty($_POST['email']) && !empty($_POST['password'])) {
            $currentUser = User::getUsersByEmail($_POST['email']);

            if (password_verify($_POST['password'], $currentUser['password'] )) {
                $_SESSION['user']['id'] = $currentUser['id'];
                $_SESSION['user']['username'] = $currentUser['username'];
                $_SESSION['user']['email'] = $currentUser['email'];

                header("Location: /");

                return true;
            }
        }

        require_once(ROOT . '/views/site/login.php');

        return true;
    }

    public function actionAccount() {
        if (!User::isGuest()) {
            $isUserAdmin = User::isAdmin($_SESSION['user']['id']);

            require_once(ROOT . '/views/site/user/account.php');
        }

        return true;
    }

    public function actionRegister() {
        if (!empty($_POST['email']) && !empty($_POST['username']) && !empty($_POST['password'])) {
            if ($attributes = self::validateRegisterData()){
                new User($attributes);
            }
        }

        require_once(ROOT . '/views/site/admin/register-form.php');

        return true;
    }

    private function validateRegisterData() {
        if(!empty($_POST['email']) && !empty($_POST['password'])
            && !empty($_POST['password2']) && !empty($_POST['username'])) {

            $passwordHash = "";
            if ($_POST['password'] == $_POST['password2']) {
                $passwordHash = password_hash($_POST['password'], PASSWORD_DEFAULT);
            } else {
                return false;
            }

            $attributes = [
                'username'=> $_POST['username'],
                'email'=> $_POST['email'],
                'password' => $passwordHash,
            ];

            return $attributes;
        }
        return false;
    }
}