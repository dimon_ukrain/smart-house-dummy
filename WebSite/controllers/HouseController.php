<?php

include_once 'models/User.php';
include_once 'models/House.php';
include_once 'models/Room.php';
include_once 'models/Device.php';
include_once 'models/House_rooms.php';
include_once 'models/Room_devices.php';
include_once 'models/Arduino.php';

class HouseController
{
    /**
     * @return bool
     */
    public function actionIndex()
    {
        if(!User::isGuest()) {
            $currentUser = User::getCurrentUser();
            $houses = House::getHousesByUserId($currentUser['id']);

            require_once(ROOT . '/views/site/houses/index.php');
        }

        return true;
    }

    /**
     * @param $houseId
     * @return bool
     */
    public function actionHouse($houseId)
    {
        if(!User::isGuest()) {
            $ownerId = User::getCurrentUser()['id'];

            if (House::getOwnerIdByHouseId($houseId) == $ownerId
                || User::isCurrentUserAdmin()) {
                $rooms = Room::getRoomsByHouseId($houseId);

                require_once(ROOT . '/views/site/houses/house.php');
            }
        }

        return true;
    }

    /**
     * @param $houseId
     * @param $roomId
     * @return bool
     */
    public function actionRoom($houseId, $roomId)
    {
        if(!User::isGuest()) {
            $ownerId = User::getCurrentUser()['id'];

            if($ownerId == Room::getOwnerIdByRoomIdAndHouseId($houseId, $roomId)) {
                $room = Room::getRoomById($roomId);
                $houseRoomId = House_rooms::getHouseRoom($houseId, $roomId)['id'];
                $devices = Device::getDevicesByHousesRoomId($houseRoomId);
                $arduinoToken = Arduino::getArduinoByHouseRoomId($houseRoomId)['token'];

                require_once(ROOT . '/views/site/room/index.php');
            }
        }

        return true;
    }

}