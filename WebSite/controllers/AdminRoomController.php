<?php

include_once 'models/User.php';
include_once 'models/Room.php';
include_once 'models/House.php';
include_once 'models/Device.php';
include_once 'models/Room_devices.php';


class AdminRoomController
{
    public function actionIndex()
    {
        if (User::isCurrentUserAdmin()) {
            $columns = Room::getColumns();
            $rooms = Room::getRooms();

            require_once(ROOT . '/views/site/admin-room/list.php');
        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionCreate()
    {
        if (User::isCurrentUserAdmin()) {

            $columns = Room::getColumns();
            $createdObject = 'room';

            if(!empty($_POST['room'])) {
                $insertAttributes = $_POST['room'];

                Room::create($insertAttributes);
            }

            require_once(ROOT . '/views/site/admin/create-form.php');


        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionEdit($id)
    {
        if (User::isCurrentUserAdmin()) {

            $columns = Room::getColumns();
            $room = Room::getRoomById($id);

            if(!empty($_POST['room'])) {
                $insertAttributes = $_POST['room'];

                Room::edit($id, $insertAttributes);
            }

            require_once(ROOT . '/views/site/admin-room/edit.php');


        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionDelete($id)
    {
        if (User::isCurrentUserAdmin()) {
            Room_devices::deleteRoom($id);
            Room::delete($id);
        } else {
            echo 'You are not admin';
        }

        return true;
    }

}