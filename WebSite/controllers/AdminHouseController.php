<?php

include_once 'models/User.php';
include_once 'models/Room.php';
include_once 'models/House.php';
include_once 'models/Device.php';
include_once 'models/House_rooms.php';
include_once 'models/Room_devices.php';

class AdminHouseController
{
    public function actionIndex()
    {
        if (User::isCurrentUserAdmin()) {
            $columns = House::getColumns();
            $houses = House::getHouses();

            require_once(ROOT . '/views/site/admin-house/list.php');
        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionCreate()
    {
        if (User::isCurrentUserAdmin()) {

            $columns = House::getColumns();
            $createdObject = 'house';


            if(!empty($_POST['house'])) {
                $insertAttributes = $_POST['house'];

                House::create($insertAttributes);
            }

            require_once(ROOT . '/views/site/admin/create-form.php');


        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionEdit($id)
    {
        if (User::isCurrentUserAdmin()) {

            $columns = House::getColumns();
            $house = House::getHouseById($id);

            if(!empty($_POST['house'])) {
                $insertAttributes = $_POST['house'];

                House::edit($id, $insertAttributes);
            }

            require_once(ROOT . '/views/site/admin-house/edit.php');


        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionDelete($id)
    {
        if (User::isCurrentUserAdmin()) {
            House_rooms::deleteHouse($id);
            House::delete($id);
        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionHouseRoomDelete($houseRoomId)
    {
        if (User::isCurrentUserAdmin()) {
            House_rooms::deleteByHouseRoomId($houseRoomId);
        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionRoomDeviceDelete($roomDeviceId)
    {
        if (User::isCurrentUserAdmin()) {
            Room_devices::deleteByRoomDeviceId($roomDeviceId);
        } else {
            echo 'You are not admin';
        }

        return true;
    }

    /**
     * @param $object
     * @param $element
     * @return bool
     */
    public function actionAddRoom($houseId)
    {
        if (User::isCurrentUserAdmin()) {
            $columns = House_rooms::getColumns();
            $house = House::getHouseById($houseId);
            $rooms = Room::getRooms();

            if(isset($_POST['room_id'])) {
                House_rooms::addRoomForHouse($houseId, $_POST['room_id']);
            }

            require_once(ROOT . '/views/site/admin-house/add-room.php');

        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionRoomsList($houseId)
    {
        if (User::isCurrentUserAdmin()) {
            $columns = Room::getColumns();
            $house = House::getHouseById($houseId);
            $rooms = Room::getRooms();
            $houseRooms = House_rooms::getRoomsByHouseId($houseId);

            require_once(ROOT . '/views/site/admin-house/rooms-list.php');

        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionDevicesList($houseRoomId)
    {
        if (User::isCurrentUserAdmin()) {
            $deviceColumns = Device::getColumns();
            $roomDeviceColumns = Room_devices::getColumns();

            $devices = Device::getDevices();

            $houseRoom = House_rooms::getHouseRoomById($houseRoomId);
            $roomDevices = Room_devices::getRoomDevicesByHouseRoomsId($houseRoomId);

            require_once(ROOT . '/views/site/admin-house/devices-list.php');

        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionAddDevice($houseRoomId)
    {
        if (User::isCurrentUserAdmin()) {
            $devices = Device::getDevices();
            if(!empty($_POST['device_id']) && !empty($_POST['device_value'])) {
                Room_devices::addDeviceForRoom($houseRoomId, $_POST['device_id'], $_POST['device_value']);
            }

            require_once(ROOT . '/views/site/admin-house/add-device.php');

        } else {
            echo 'You are not admin';
        }

        return true;
    }

    public function actionEditRoomDevice($roomDeviceId)
    {
        if (User::isCurrentUserAdmin()) {
            $devices = Device::getDevices();
            $roomDevice = Room_devices::getRoomDeviceById($roomDeviceId);
            $currentDevice = Device::getDeviceById($roomDevice['device_id']);

            if(!empty($_POST['device_value'])) {
                Room_devices::edit($roomDevice['id'], $_POST['device_value']);
            }

            require_once(ROOT . '/views/site/admin-house/edit-device.php');

        } else {
            echo 'You are not admin';
        }

        return true;
    }

}