<?php

include_once 'models/User.php';

class SiteController
{
    public function actionIndex() {
        if(isset($_SESSION['user'])) {
            //require_once(ROOT . '/views/site/houses/list.php');
            header("Location: /houses");

        } else {
            require_once(ROOT . '/views/site/login.php');
        }

        return true;
    }

    public function actionHouses() {
        if(!User::isGuest()) {
            require_once(ROOT . '/views/site/houses/index.php');
        }

        return true;
    }

}