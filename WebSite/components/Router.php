<?php


class Router
{
    private $routes;
    protected static $instance = null;

    private function __construct()
    {
        $routesPath = ROOT.'/config/routes.php';
        $this->routes = include($routesPath);

    }

    protected function __clone()
    {
        //Тут какой-то код
        throw new Exception('Feature disabled.');
    }

    protected function __wakeup()
    {
        //Тут какой-то код
        throw new Exception('Feature disabled.');
    }

    public static function getInstance()
    {
        if (static::$instance == null)
        {
            static::$instance = new static;
        }

        return static::$instance;
    }

    public static function debug($string) {
        echo '<pre>';
            print_r($string);
        echo '</pre>';
    }

    private function getURI(){
        if(!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run() {
        $uri = $this->getURI();
        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)){

                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                $segments = explode('/', $internalRoute);

                $controllerName = array_shift($segments).'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action' . ucfirst(array_shift($segments));

                $parameters = $segments;

                $controllerFile = ROOT .'/controllers/' .
                    $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }

                $controllerObject = new $controllerName;

                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                if ($result != null) {
                    break;
                }
            }
        }
    }
}