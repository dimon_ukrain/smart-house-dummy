<?php

return [

    ///USER
    //Registration form
    'register' => 'user/register',
    //Login
    'login' => 'user/login',
    //Account
    'logout' => 'user/logout',
    'account' => 'user/account',

    //Arduino
    'arduino/send/request' => 'arduino/arduinoSendRequest',
    'arduino/send/response' => 'arduino/arduinoSendResponse',
    'arduino/devices/update' => 'arduino/updateDevices',

    //Admin-arduino
    'admin/arduino/list' => 'adminArduino/index',
    'admin/arduino/delete/(\d+)' => 'adminArduino/delete/$1',
    'admin/arduino/create' => 'adminArduino/create',
    'admin/arduino/edit/(\d+)' => 'adminArduino/edit/$1',

    //Admin-house
    'admin/houseRoom/(\d+)/add/device' => 'adminHouse/addDevice/$1',
    'admin/house/(\d+)/roomsList' => 'adminHouse/roomsList/$1',
    'admin/houseRoom/(\d+)/devicesList' => 'adminHouse/devicesList/$1',

    'admin/houseRoom/delete/(\d+)' => 'adminHouse/houseRoomDelete/$1',
    'admin/roomDevice/delete/(\d+)' => 'adminHouse/roomDeviceDelete/$1',
    'admin/roomDevice/edit/(\d+)' => 'adminHouse/editRoomDevice/$1',

    'admin/house/(\d+)/add/room' => 'adminHouse/addRoom/$1',
    'admin/house/edit/(\d+)' => 'adminHouse/edit/$1',
    'admin/house/create' => 'adminHouse/create',
    'admin/house/delete/(\d+)' => 'adminHouse/delete/$1',
    'admin/house/list' => 'adminHouse/index',

    //Admin-room
    'admin/room/edit/(\d+)' => 'adminRoom/edit/$1',
    'admin/room/create' => 'adminRoom/create',
    'admin/room/delete/(\d+)' => 'adminRoom/delete/$1',
    'admin/room/list' => 'adminRoom/index',

    //Admin-device
    'admin/device/edit/(\d+)' => 'adminDevice/edit/$1',
    'admin/device/create' => 'adminDevice/create',
    'admin/device/delete/(\d+)' => 'adminDevice/delete/$1',
    'admin/device/list' => 'adminDevice/index',
    'admin' => 'admin/index',

    //House
    'house/(\d+)/room/(\d+)' => 'house/room/$1/$2',
    'house/(\d+)' => 'house/house/$1',
    'houses' => 'house/index',


    '' => 'site/index',
];
