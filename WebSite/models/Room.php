<?php

class Room
{
    private $id;
    private $name;
    private $room_icon;
    private $description;

    static function getColumns() {
        return [
            ["COLUMN_NAME" => "id"],
            ["COLUMN_NAME" => "name"],
            ["COLUMN_NAME" => "img_src"],
            ["COLUMN_NAME" => "description"],
        ];
    }

    static function getRooms() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from rooms');

        $result->setFetchMode(PDO::FETCH_ASSOC);

        return $result->fetchAll();
    }

    static function getRoomById($id) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from rooms WHERE id = ' . $id);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetch();

        return $houseItem;
    }

    static function getRoomByName($name) {
        $db = Db::getConnection();

        $result = $db->query("SELECT * from rooms WHERE name = " . "'" . $name . "'");

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetch();

        return $houseItem;
    }

    static function getRoomsByHouseId($houseId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT house_rooms.id AS houseRoomId, rooms.* from house_rooms '
        . 'INNER JOIN rooms ON house_rooms.room_id = rooms.id '
        . 'INNER JOIN houses ON house_rooms.house_id = houses.id '
        . 'WHERE house_id = ' . $houseId);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetchAll();

        return $houseItem;
    }

    static function getOwnerIdByRoomIdAndHouseId($houseId, $roomId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT houses.owner_id from house_rooms '
            . 'INNER JOIN rooms ON house_rooms.room_id = rooms.id '
            . 'INNER JOIN houses ON house_rooms.house_id = houses.id '
            . 'WHERE house_rooms.room_id = ' . $roomId
            . ' AND house_rooms.house_id = ' . $houseId);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetch();

        return $houseItem['owner_id'];
    }

    static function delete($id) {
        $db = Db::getConnection();
        $query = "DELETE FROM rooms WHERE id = " . $id;

        return $result = $db->query($query);
    }

    static function edit($id, $insertAttributes) {
        $db = Db::getConnection();

        $houseColumns = '';
        $houseValues = '';

        $setValues = ' SET ';
        foreach ($insertAttributes as $insertColumn => $insertValue) {
            $setValues.= $insertColumn . '=' . "'{$insertValue}'" . ',';
        }
        $setValues = trim($setValues, ',');

        $query = "UPDATE rooms";
        $query .= $setValues;
        $query .=" WHERE id = {$id}";

        return $result = $db->query($query);
    }

    static function create($insertAttributes) {
        $db = Db::getConnection();

        $houseColumns = '';
        $houseValues = '';
        foreach ($insertAttributes as $insertColumn => $insertValue) {
            $houseColumns .= ' ' . $insertColumn . ',';
            $houseValues .=  " '" . $insertValue . "',";
        }

        $houseColumns = trim(trim($houseColumns, ','), ' ');
        $houseValues = trim(trim($houseValues, ','), ' ');

        $query = "INSERT INTO rooms ({$houseColumns})VALUES({$houseValues})";

        return $result = $db->query($query);
    }

}