<?php


class User
{
    public function __construct($attributes)
    {
        $db = Db::getConnection();

        $sql = "INSERT INTO users (username, password, email)
        VALUES (:username, :password, :email)";

        $result = $db->prepare($sql);

        $result->bindParam(':username', $attributes['username']);
        $result->bindParam(':password', $attributes['password']);
        $result->bindParam(':email', $attributes['email']);

       return $result->execute();
    }

    static public function getUsers() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from users');
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $userItem = $result->fetchAll();

        return $userItem;
    }

    static public function getUsersByEmail($email) {
        $db = Db::getConnection();

        $result = $db->query("SELECT * from users WHERE email = " . "'" . $email . "'");
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $userItem = $result->fetch();

        return $userItem;
    }

    static public function getUserById($id) {
        $db = Db::getConnection();

        $result = $db->query("SELECT * from users WHERE id =" . $id);
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $userItem = $result->fetch();

        return $userItem;
    }

    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }

        return true;
    }

    public static function getCurrentUser()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }

        return false;
    }

    public static function isCurrentUserAdmin()
    {
        if (self::isAdmin(self::getCurrentUser()['id'])) {
            return true;
        } else {
            return false;
        }

    }

    public static function isAdmin($userId)
    {
        $db = Db::getConnection();

        $result = $db->query("SELECT role from users WHERE id =" . $userId);
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $userItem = $result->fetch();

        if ($userItem['role'] == 'admin') {
            return true;
        } else {
            return false;
        }
    }

    static function getHousesByUserId($userId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from houses WHERE owner_id = ' . $userId);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetchAll();

        return $houseItem;
    }


}