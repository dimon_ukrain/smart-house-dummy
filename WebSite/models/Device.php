<?php

class Device
{
    static function getColumns() {
        return [
            ["COLUMN_NAME" => "id"],
            ["COLUMN_NAME" => "name"],
            ["COLUMN_NAME" => "short_description"],
            ["COLUMN_NAME" => "description"],
            ["COLUMN_NAME" => "output_type_id"],
        ];
    }

    static function getDevices() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from devices');

        $result->setFetchMode(PDO::FETCH_ASSOC);

        return $result->fetchAll();
    }

    static function getDeviceById($id) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from devices WHERE id = ' . $id);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetch();

        return $houseItem;
    }

    /**
     * @param $housesRoomId
     * @return array
     */
    static function getDevicesByHousesRoomId($housesRoomId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT room_devices.id AS roomDeviceId, room_devices.device_value, devices.* from room_devices '
        . 'INNER JOIN devices ON room_devices.device_id = devices.id '
        . 'INNER JOIN house_rooms ON room_devices.house_rooms_id = house_rooms.id '
        . 'WHERE house_rooms.id = ' . $housesRoomId);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetchAll();

        return $houseItem;
    }

    static function getDeviceIdByName($deviceName) {
        $db = Db::getConnection();

        $result = $db->query("SELECT id from devices WHERE name = " . "'{$deviceName}'");

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetch();

        return $houseItem;
    }

    static function delete($id) {
        $db = Db::getConnection();
        $query = "DELETE FROM devices WHERE id = " . $id;

        return $result = $db->query($query);
    }

    static function edit($id, $insertedAttributes) {
        $db = Db::getConnection();

        $query = "UPDATE devices"
            . " SET name = :name, short_description = :short_description,"
            . " description = :description, output_type_id = :output_type_id"
            ." WHERE id = :id";

        $sth = $db->prepare($query);

        $sth->bindParam(':name', $insertedAttributes['name'], PDO::PARAM_STR);
        $sth->bindParam(':short_description',
            $insertedAttributes['short_description'], PDO::PARAM_STR);
        $sth->bindParam(':description',
            $insertedAttributes['description'], PDO::PARAM_STR);
        $sth->bindParam(':output_type_id',
            $insertedAttributes['output_type_id'], PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        return $sth->execute();
    }

    static function create($insertAttributes) {
        $db = Db::getConnection();

        $deviceColumns = '';
        $deviceValues = '';
        foreach ($insertAttributes as $insertColumn => $insertValue) {
            $deviceColumns .= ' ' . $insertColumn . ',';
            $deviceValues .=  " '" . $insertValue . "',";
        }

        $deviceColumns = trim(trim($deviceColumns, ','), ' ');
        $deviceValues = trim(trim($deviceValues, ','), ' ');

        $query = "INSERT INTO devices ({$deviceColumns})VALUES({$deviceValues})";

        return $result = $db->query($query);
    }
}