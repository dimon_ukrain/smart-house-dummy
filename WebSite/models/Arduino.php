<?php

class Arduino
{
    static function getColumns() {
        return [
            ["COLUMN_NAME" => "id"],
            ["COLUMN_NAME" => "house_room_id"],
            ["COLUMN_NAME" => "token"],
        ];
    }

    static function getArduinos() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from arduino');

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    static function getArduinoById($id) {
        $db = Db::getConnection();

        $sth = $db->prepare('SELECT * from arduino WHERE id = :id');
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    static function getArduinoByHouseRoomId($houseRoomId) {
        $db = Db::getConnection();

        $sth = $db->prepare('SELECT * from arduino WHERE id = :id');

        $sth->bindParam(':id', $houseRoomId, PDO::PARAM_INT);

        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    static function delete($id) {
        $db = Db::getConnection();
        $query = 'DELETE FROM arduino WHERE id = :id';

        $sth = $db->prepare($query);

        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        $sth->execute();
    }

    static function edit($id, $insertedAttributes) {
        $db = Db::getConnection();

        $query = "UPDATE arduino"
            . " SET house_room_id = :house_room_id, token = :token"
            ." WHERE id = :id";

        $sth = $db->prepare($query);

        $sth->bindParam(':house_room_id', $insertedAttributes['house_room_id'], PDO::PARAM_INT);
        $sth->bindParam(':token', $insertedAttributes['token'], PDO::PARAM_STR );
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        return $sth->execute();
    }

    static function create($insertedAttributes) {
        $db = Db::getConnection();

        $query = "INSERT INTO arduino (house_room_id, token)".
            "VALUES(:house_room_id, :token)";

        $sth = $db->prepare($query);

        $sth->bindParam(':house_room_id', $insertedAttributes['house_room_id'], PDO::PARAM_INT);
        $sth->bindParam(':token', $insertedAttributes['token'], PDO::PARAM_STR );

        return $sth->execute();
    }
}