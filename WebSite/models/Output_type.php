<?php

class Output_type
{
    static function getColumns() {
        return [
            ["COLUMN_NAME" => "id"],
            ["COLUMN_NAME" => "type"],
            ["COLUMN_NAME" => "img_src"],
        ];
    }

    static function getTypes() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from output_types');

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    static function getTypeById($id) {
        $db = Db::getConnection();

        $sth = $db->prepare('SELECT * from output_types WHERE id = :id');
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    static function delete($id) {
        $db = Db::getConnection();
        $query = 'DELETE FROM output_types WHERE id = :id';

        $sth = $db->prepare($query);

        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        $sth->execute();
    }

    static function edit($id, $insertedAttributes) {
        $db = Db::getConnection();

        $query = "UPDATE output_types"
            . " SET type = :type, img_src = :img_src"
            ." WHERE id = :id";

        $sth = $db->prepare($query);

        $sth->bindParam(':type', $insertedAttributes['type'], PDO::PARAM_STR );
        $sth->bindParam(':img_src', $insertedAttributes['img_src'], PDO::PARAM_STR );
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        return $sth->execute();
    }

    static function create($insertedAttributes) {
        $db = Db::getConnection();

        $query = "INSERT INTO output_types (type, img_src)".
            "VALUES(:type, :img_src)";

        $sth = $db->prepare($query);

        $sth->bindParam(':type', $insertedAttributes['type'], PDO::PARAM_INT);
        $sth->bindParam(':img_src', $insertedAttributes['img_src'], PDO::PARAM_INT);

        return $sth->execute();
    }
}