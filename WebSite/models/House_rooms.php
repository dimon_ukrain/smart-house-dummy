<?php


class House_rooms
{
    static function getColumns() {
        return [
            ["COLUMN_NAME" => "id"],
            ["COLUMN_NAME" => "house_id"],
            ["COLUMN_NAME" => "room_id"],
        ];
    }

    static function getHouseRoomById($id) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from house_rooms WHERE id = ' . $id);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetch();

        return $houseItem;
    }

    static function getRoomsByHouseId($id) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from house_rooms WHERE house_id = ' . $id);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetchAll();

        return $houseItem;
    }

    static function getHouseRoom($housesId, $roomId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT house_rooms.* from house_rooms '
            . 'INNER JOIN rooms ON house_rooms.room_id = rooms.id '
            . 'INNER JOIN houses ON house_rooms.house_id = houses.id '
            . 'WHERE houses.id = ' . $housesId
            . ' AND rooms.id = ' . $roomId);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseRoomItem = $result->fetch();

        return $houseRoomItem;
    }

    static function deleteHouse($houseId) {
        $db = Db::getConnection();
        $query = "DELETE FROM house_rooms WHERE house_id =" . $houseId;

        return $result = $db->query($query);
    }

    static function deleteByHouseRoomId($houseRoomId) {
        $db = Db::getConnection();
        $query = "DELETE FROM house_rooms WHERE id =" . $houseRoomId;

        return $result = $db->query($query);
    }

    static function addRoomForHouse($houseId, $roomId) {
        $db = Db::getConnection();
        $query = "INSERT INTO house_rooms (house_id, room_id)VALUES('{$houseId}','{$roomId}')";

        return $result = $db->query($query);
    }


    static function create($insertAttributes) {
        $db = Db::getConnection();

        $columns = '';
        $values = '';
        foreach ($insertAttributes as $insertColumn => $insertValue) {
            $columns .= ' ' . $insertColumn . ',';
            $values .=  " '" . $insertValue . "',";
        }

        $columns = trim(trim($columns, ','), ' ');
        $values = trim(trim($values, ','), ' ');

        $query = "INSERT INTO house_rooms ({$columns})VALUES({$values})";

        return $result = $db->query($query);
    }
}