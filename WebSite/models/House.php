<?php

class House
{
    private $id;
    private $description;
    private $owner_id;

    static function getColumns() {
        return [
            ["COLUMN_NAME" => "id"],
            ["COLUMN_NAME" => "owner_id"],
            ["COLUMN_NAME" => "description"],
            ["COLUMN_NAME" => "room_amount"],
        ];
    }

    static function getHouses() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from houses');

        $result->setFetchMode(PDO::FETCH_ASSOC);

        return $result->fetchAll();
    }

    static function getHouseById($id) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from houses WHERE id = ' . $id);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetch();

        return $houseItem;
    }

    static function getHousesByUserId($userId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from houses WHERE owner_id = ' . $userId);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetchAll();

        return $houseItem;
    }

    static function getOwnerIdByHouseId($housesId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT owner_id from houses WHERE id = ' . $housesId);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetch();

        return $houseItem['owner_id'];
    }

    static function create($insertAttributes) {
        $db = Db::getConnection();

        $houseColumns = '';
        $houseValues = '';
        foreach ($insertAttributes as $insertColumn => $insertValue) {
            $houseColumns .= ' ' . $insertColumn . ',';
            $houseValues .=  " '" . $insertValue . "',";
        }

        $houseColumns = trim(trim($houseColumns, ','), ' ');
        $houseValues = trim(trim($houseValues, ','), ' ');

        $query = "INSERT INTO houses ({$houseColumns})VALUES({$houseValues})";

        return $result = $db->query($query);
    }

    static function delete($id) {
        $db = Db::getConnection();
        $query = "DELETE FROM houses WHERE id = " . $id;

        return $result = $db->query($query);
    }

    static function edit($id, $insertAttributes) {
        $db = Db::getConnection();

        $houseColumns = '';
        $houseValues = '';

        $setValues = ' SET ';
        foreach ($insertAttributes as $insertColumn => $insertValue) {
            $setValues.= $insertColumn . '=' . "'{$insertValue}'" . ',';
        }
        $setValues = trim($setValues, ',');

        $query = "UPDATE houses";
        $query .= $setValues;
        $query .=" WHERE id = {$id}";

        return $result = $db->query($query);
    }

}