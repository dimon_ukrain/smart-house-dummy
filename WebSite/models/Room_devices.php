<?php

class Room_devices
{
    static function getColumns() {
        return [
            ["COLUMN_NAME" => "id"],
            ["COLUMN_NAME" => "house_rooms_id"],
            ["COLUMN_NAME" => "device_id"],
            ["COLUMN_NAME" => "device_value"],
        ];
    }

    static function getRoomDeviceById($id) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from room_devices WHERE id = ' . $id);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetch();

        return $houseItem;
    }

    static function edit($id, $deviceValue) {
        $db = Db::getConnection();

        $query = "UPDATE room_devices"
            . " SET device_value = :device_value"
            ." WHERE id = :id";

        $sth = $db->prepare($query);

        $sth->bindParam(':device_value',
            $deviceValue, PDO::PARAM_STR);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);

        return $sth->execute();
    }

    static function getRoomDevicesByDeviceId($deviceId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from room_devices WHERE device_id = ' . $deviceId);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetchAll();

        return $houseItem;
    }

    static function getRoomDevicesByHouseRoomsId($houseRoomsId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * from room_devices WHERE house_rooms_id  = ' . $houseRoomsId);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $houseItem = $result->fetchAll();

        return $houseItem;
    }

    static function updateDeviceValue($deviceRoomId, $deviceValue) {
        $db = Db::getConnection();

        $query = "UPDATE room_devices SET device_value = '{$deviceValue}' " .
            "WHERE id = '{$deviceRoomId}'";

        return $result = $db->query($query);
    }

    static function addDeviceForRoom($houseRoomsId, $deviceId, $deviceValue) {
        $db = Db::getConnection();
        $query = "INSERT INTO room_devices (house_rooms_id, device_id, device_value)VALUES('{$houseRoomsId}','{$deviceId}','{$deviceValue}')";

        return $result = $db->query($query);
    }

    static function deleteRoom($roomId) {
        $db = Db::getConnection();
        $query = "DELETE FROM room_devices WHERE room_id =" . $roomId;

        return $result = $db->query($query);
    }

    static function deleteDevices($device_id) {
        $db = Db::getConnection();
        $query = "DELETE FROM room_devices WHERE device_id =" . $device_id;

        return $result = $db->query($query);
    }

    static function deleteByRoomDeviceId($roomDeviceId) {
        $db = Db::getConnection();
        $query = "DELETE FROM room_devices WHERE id =" . $roomDeviceId;

        return $result = $db->query($query);
    }

    static function create($insertAttributes) {
        $db = Db::getConnection();

        $columns = '';
        $values = '';
        foreach ($insertAttributes as $insertColumn => $insertValue) {
            $columns .= ' ' . $insertColumn . ',';
            $values .=  " '" . $insertValue . "',";
        }

        $columns = trim(trim($columns, ','), ' ');
        $values = trim(trim($values, ','), ' ');

        $query = "INSERT INTO room_devices ({$columns})VALUES({$values})";

        return $result = $db->query($query);
    }
}