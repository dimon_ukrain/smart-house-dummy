<?php

require_once ROOT . '/views/layouts/header.php'; ?>


<form method="post" id="createForm" action="/admin/room/edit/<?php echo $room['id']; ?>" class="create">
    <div class="create-block">
        <h1>Edit room</h1>
        <p>Please fill in this form to edit an <?php echo $room['description']; ?>.</p>
        <hr>

        <?php foreach($columns as $column): ?>
            <?php if($column['COLUMN_NAME'] != 'id'): ?>

                <input type="text"
                       name="room[<?php echo $column['COLUMN_NAME'];?>]"
                       placeholder="<?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?>"
                       value="<?php echo $room[$column['COLUMN_NAME']]; ?>"/>
            <?php endif;?>
        <?php endforeach; ?>

        <input type="submit" value="Send form"/>
    </div>
</form>

<script>
    $("#createForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                location.replace("/admin/room/list");
            }
        });
    });
</script>