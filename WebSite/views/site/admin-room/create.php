<?php

require_once ROOT . '/views/layouts/header.php'; ?>


<form method="post" id="createForm" action="/admin/create-<?php echo $createdObject;?>" class="create">
    <div class="create-block">
        <h1>Create House</h1>
        <p>Please fill in this form to create an House.</p>
        <hr>

        <?php foreach($columns as $column): ?>
            <?php if($column['COLUMN_NAME'] != 'id'): ?>
                <input type="text" name="house[<?php echo $column['COLUMN_NAME'];?>]" placeholder="<?php echo $column['COLUMN_NAME'];?>"/>
            <?php endif;?>
        <?php endforeach; ?>

        <input type="submit" value="Send form"/>
    </div>
</form>

<a href="/admin/room/list" class="btn btn-light">Return to rooms list</a>

<script>
    $("#createForm").submit(function(e) {

        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                $('#createForm')[0].reset();
            }
        });


    });
</script>