<?php require_once ROOT . '/views/layouts/header.php'; ?>
<div class="admin-house-list">
    <table id="rooms" class="table table-striped">
        <thead>
        <tr>
            <?php foreach($columns as $column): ?>
                <th><?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?></th>
            <?php endforeach; ?>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($rooms as $room): ?>
        <tr>
            <?php foreach($columns as $column): ?>
                <td><?php echo $room[$column['COLUMN_NAME']]; ?></td>
            <?php endforeach; ?>
            <td>
                <a href="/admin/room/edit/<?php echo $room['id']?>" class="btn btn-light">Edit</a>
                <a href="/admin/room/delete/<?php echo $room['id']?>" class="btn btn-danger delete-article" data-id="<?php echo $room['id']?>">Delete</a>

            </td>
        </tr>

        <?php endforeach; ?>
        </tbody>

    </table>

    <div class="create-link">
        <a href="/admin/room/create"><h3 class="btn btn-primary">Create Room</h3></a>
    </div>
    <div class="create-link">
        <a href="/admin" class="btn btn-light">Return to admin</a>
    </div>
</div>


