<?php require_once ROOT . '/views/layouts/header.php'; ?>
    <div class="smart-house-rooms">
        <h2>Кімнати:</h2>
        <div class="house-rooms-wrapper">
        <?php foreach($rooms as $room): ?>
        <a href="/house/<?php echo $houseId;?>/room/<?php echo $room['id'];?>" class="smart-house-room">
            <div class="img-room">
                <img src="../../template/img/<?php echo $room['img_src']?>" alt="">
            </div>
            <div class="room-info">
                <h3><?php echo $room['name']?></h3>
                <p><?php echo count(Device::getDevicesByHousesRoomId($room['houseRoomId'])); ?> devices</p>
            </div>

        </a>
        <?php endforeach;?>

        </div>
    </div>

<script src="../../template/js/tools.js"></script>
<script src="https://kit.fontawesome.com/8cae3463cf.js" crossorigin="anonymous"></script>