<?php require_once ROOT . '/views/layouts/header.php'; ?>

<?php /*<div class="user-houses">
    <?php foreach($houses as $house): ?>
    <a href="/house/<?php echo $house['id']?>" class="user-house">
        <h3><?php echo $house['description']?></h3>
        <img src="../../template/img/house.png" alt="">
        <p><?php echo $currentUser['username']; ?></p>
    </a>
    <?php endforeach;?>
</div> */?>

<table class="user-houses">
    <tbody>
    <tr>
        <td><h2>Квартири</h2></td>
    </tr>

    <?php foreach($houses as $house): ?>
    <tr class="wrapper">
        <td>
            <div class="house-wrap">
                <table width="100%" cellspacing="0" cellpadding="0" >
                    <tbody>
                    <tr>
                        <td width="150" rowspan="2" class="photo-cell">
                            <a href="/house/<?php echo $house['id']?>" class="house-link">
                                <img src="../../template/img/house.png" alt="">
                            </a>
                        </td>
                        <td  valign="top">
                            <div class="space house-description">
                                <h3><?php echo $house['description']?></h3>
                                <p>Кількість кімнат: <?php echo count(Room::getRoomsByHouseId($house['id'])); ?></p>

                            </div>
                        </td>
                        <td class="house-owner">
                            <p><strong>Власник:</strong> <?php echo $currentUser['username']; ?></p>
                            <p><strong>ID:</strong> <?php echo $house['id']; ?></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>