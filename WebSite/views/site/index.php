<?php require_once ROOT . '/views/layouts/header.php'; ?>

<main>
    <div class="nav-bar">
        <h3>Датчики та пристрої</h3>
        <a href="">Годинник реального часу</a>
        <a href="">Датчики вологості</a>
        <a href="">Датчики температури</a>
        <a href="">Датчики газу</a>
        <a href="">Датчик атмосферного тиску</a>
    </div>

    <div class="smart-house-rooms">

        <a href="/room/living-room" class="smart-house-room living-room">
            <div class="img-room">
                <img src="../../template/img/Icons_bigWeb.png" alt="">
            </div>
            <h3>Living room</h3>
            <p>6 devices</p>
        </a>

        <a href="/room/kitchen" class="smart-house-room kitchen">
            <div class="img-room">
                <img src="../../template/img/Icons_bigWeb.png" alt="">
            </div>
            <h3>Kitchen</h3>
            <p>6 devices</p>
        </a>

        <a href="/room/bedroom" class="smart-house-room bedroom">
            <div class="img-room">
                <img src="../../template/img/Icons_bigWeb.png" alt="">
            </div>
            <h3>Bedroom</h3>
            <p>6 devices</p>
        </a>

        <a href="/room/washing-room" class="smart-house-room washing-room">
            <div class="img-room">
                <img src="../../template/img/Icons_bigWeb.png" alt="">
            </div>
            <h3>Washing room</h3>
            <p>6 devices</p>
        </a>

        <a href="/room/bathroom" class="smart-house-room bathroom">
            <div class="img-room">
                <img src="../../template/img/Icons_bigWeb.png" alt="">
            </div>
            <h3>Bathroom</h3>
            <p>6 devices</p>
        </a>

        <a href="/room/family-room" class="smart-house-room family-room">
            <div class="img-room">
                <img src="../../template/img/Icons_bigWeb.png" alt="">
            </div>
            <h3>Family room</h3>
            <p>6 devices</p>
        </a>

    </div>
</main>

<script src="../../template/js/tools.js"></script>
<script src="https://kit.fontawesome.com/8cae3463cf.js" crossorigin="anonymous"></script>