<?php require_once ROOT . '/views/layouts/header.php'; ?>

<div class="login-modal">
    <form class="modal-content" action="/login" method="post">
        <div class="imgcontainer">
            <img src="template/img/img_avatar2.png" alt="Avatar" class="avatar">
        </div>

        <div class="container">
            <label for="email"><b>Email</b></label>
            <input type="email" placeholder="Enter email" name="email" required>

            <label for="password"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" required>

            <button type="submit">Login</button>
            <label>
                <input type="checkbox" checked="checked" name="remember"> Remember me
            </label>
        </div>

        <div class="container sign-up">
            <p>Create account<a href="/register"> Sign up</a>.</p>
        </div>
    </form>
</div>
