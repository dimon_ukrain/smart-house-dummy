<?php

require_once ROOT . '/views/layouts/header.php'; ?>


<form method="post" id="createForm" name="<?php echo $element;?>[]" action="/admin/<?php echo $element;?>/add" class="create">
    <div class="create-block">
        <h1>Add <?php echo $element;?> to <?php echo $object;?></h1>
        <p>Please fill in this form to add an <?php echo $element;?> to <?php echo $object;?>.</p>
        <hr>

        <?php foreach($formColumns as $formColumn): ?>
            <?php if($formColumn['COLUMN_NAME'] == 'house_id'): ?>

                <?php $houses = House::getHouses(); ?>
                House: <select name="house_id" id="">
                    <?php foreach($houses as $house): ?>
                         <option value="<?php echo $house['id']?>"><?php echo $house['description']?></option>
                    <?php endforeach; ?>
                </select>

            <?php elseif($formColumn['COLUMN_NAME'] == 'room_id'): ?>
                <?php $rooms = Room::getRooms(); ?>
                Room: <select name="room_id" id="">
                    <?php foreach($rooms as $room): ?>
                        <option value="<?php echo $room['id']?>"><?php echo $room['name']?></option>
                    <?php endforeach; ?>
                </select>

            <?php elseif($formColumn['COLUMN_NAME'] == 'device_id'): ?>
                <?php $devices = Device::getDevices();; ?>
                Device: <select name="device_id" id="">
                    <?php foreach($devices as $device): ?>
                        <option value="<?php echo $device['id']?>"><?php echo $device['name']?></option>
                    <?php endforeach; ?>
                </select>

            <?php elseif($formColumn['COLUMN_NAME'] != 'id'): ?>
                <input type="text" name="<?php echo $formColumn['COLUMN_NAME']; ?>" placeholder="<?php echo $formColumn['COLUMN_NAME'];?>"/>
            <?php endif;?>
        <?php endforeach; ?>

        <input type="submit" value="Send form"/>
    </div>
</form>

<script>
    $("#createForm").submit(function(e) {

        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),

            // serializes the form's elements.
            success: function(data)
            {
                console.log(data);
                $('#createForm')[0].reset();
            }
        });


    });
</script>