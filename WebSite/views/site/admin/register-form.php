<?php require_once ROOT . '/views/layouts/header.php'; ?>


<form method="post" id="registerUser" action="/register" class="register">
    <div class="register-block">
        <h1>Register</h1>
        <p>Please fill in this form to create an account.</p>
        <hr>

        <input type="text" name="username" placeholder="Login" />
        <input type="email" name="email" placeholder="E-mail" />
        <input type="password" autocomplete="password" name="password" placeholder="Password" />
        <input type="password" autocomplete="password" name="password2" placeholder="Retype password" />

        <input type="submit" name="signup_submit" value="Sign me up" />
    </div>

    <div class="container signin">
        <p>Already have an account? <a href="/login" class="btn btn-light">Sign in</a>.</p>
    </div>
</form>


<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content modal-success">
        <span class="close">&times;</span>
        <p>Operation successfully completed</p>
    </div>

</div>

<script>
    $("#registerUser").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        let form = $(this);
        let url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                $('#registerUser')[0].reset();
                modal();
            }
        });
    });
</script>