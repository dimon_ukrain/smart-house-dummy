<?php

require_once ROOT . '/views/layouts/header.php'; ?>


<form method="post" id="createForm" action="/admin/house/edit/<?php echo $house['id']; ?>" class="create">
    <div class="create-block">
        <h1>Edit house</h1>
        <p>Please fill in this form to edit an <?php echo $house['description']; ?>.</p>
        <hr>

        <?php foreach($columns as $column): ?>
            <?php if($column['COLUMN_NAME'] != 'id'): ?>

                <input type="text"
                       name="house[<?php echo $column['COLUMN_NAME'];?>]"
                       placeholder="<?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?>"
                       value="<?php echo $house[$column['COLUMN_NAME']]; ?>"/>
            <?php endif;?>
        <?php endforeach; ?>

        <input type="submit" value="Send form"/>
    </div>
</form>

<script>
    $("#createForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                location.replace("/admin/house/list");
            }
        });
    });
</script>