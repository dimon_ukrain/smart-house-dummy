<?php require_once ROOT . '/views/layouts/header.php'; ?>

<main>
    <div class="house-room">
        <img src="/template/img/<?php echo $room['img_src']?>" alt="">
        <h3><?php echo $room['name']; ?></h3>
        <p><?php echo count($devices); ?> devices</p>

        <button onclick="turnOffAllDevices();">turn off all devices</button>
    </div>
</main>

<div class="devices-control">
    <?php foreach($devices as $device): ?>

    <div class="device-control">

        <div class="room-device-id" hidden><p><?php echo $device['roomDeviceId'];?></p></div>

        <div class="device-name"><?php echo $device['name'];?></div>

        <div class="device-tool">
            <?php if ($device['output_type_id'] == '1'):?>
                <div class="display_info">
                    <?php echo $device['device_value']; ?><sup>o</sup>
                </div>
            <?php elseif($device['output_type_id'] == '2'):?>
                <button class="<?php echo ($device['device_value'] == 'on') ? 'button_on' : 'button_off'; ?>"><?php echo $device['device_value']; ?></button>
            <?php elseif($device['output_type_id'] == '3'):?>
                <div class="switch-btn <?php echo ($device['device_value'] == 'on') ?
                    'switch-on' : ''; ?> ">
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php endforeach;?>
</div>

<script>
    let devicesControl = document.querySelector('.devices-control');

    devicesControl.onclick = function(event) {
        let target = event.target;

        if (target.classList.contains('switch-btn')) {
            controlSwitchOnClicked(target);
        }

        else if (target.className == 'button_on' || target.className == 'button_off') {
            controlButtonClicked(target);
        }

        if (target.classList.contains('switch-btn')
            || target.className == 'button_off'
            || target.className == 'button_on') {

            sendArduinoValues(target);
        }
    }

    function sendArduinoValues(element) {
        let arduinoValues = {};
        let deviceValue = element.outerText.toLowerCase();
        let deviceRoomId = element.parentElement.parentElement.querySelector('.room-device-id').outerText;
        let houseRoomId = <?php echo $houseRoomId; ?>;
        let arduinoToken = "<?php echo $arduinoToken; ?>";

        if (element.classList.contains('switch-btn')){
            if (element.classList.contains('switch-on')){
                deviceValue = 'on';
            } else {
                deviceValue = 'off';
            }
        }

        arduinoValues['id'] = deviceRoomId;
        arduinoValues['value'] = deviceValue;

        $.ajax({
            type: "post",
            url: "/arduino/devices/update",
            dataType: 'json',
            data:  {
                arduinoValues,
                houseRoomId: houseRoomId,
                token: arduinoToken
            },

            success: function (ans) {
                console.log(ans);
            }
        })
    }

    setTimeout('window.location.reload();', 30000);
</script>