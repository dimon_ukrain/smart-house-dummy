<?php

require_once ROOT . '/views/layouts/header.php'; ?>


<form method="post" id="createForm" action="/admin/device/create" class="create">
    <div class="create-block">
        <h1>Create Device</h1>
        <p>Please fill in this form to create an Device.</p>
        <hr>

        <?php foreach($columns as $column): ?>
            <?php if($column['COLUMN_NAME'] == 'description'):?>
                <textarea name="device[<?php echo $column['COLUMN_NAME'];?>]"
                          placeholder="<?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?>"
                          cols="30" rows="10"></textarea>

                <?php elseif($column['COLUMN_NAME'] == 'output_type_id'): ?>
                <div class="device-output-type">
                    <label for="device[<?php echo $column['COLUMN_NAME'];?>]">Output type: </label>
                    <select id="output_type_id" name="device[<?php echo $column['COLUMN_NAME'];?>]">
                        <?php foreach(Output_type::getTypes() as $type): ?>
                            <option value="<?php echo $type['id']?>"><?php echo $type['type'];?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <img src="../../../template/img/output-types/" id="output-type-img" alt="">

                <?php elseif($column['COLUMN_NAME'] != 'id'): ?>
                    <input type="text"
                           name="device[<?php echo $column['COLUMN_NAME'];?>]"
                           placeholder="<?php echo $column['COLUMN_NAME'];?>"/>
            <?php endif;?>
        <?php endforeach; ?>

        <input type="submit" value="Send form"/>
    </div>
</form>

<a href="/admin/device/list" class="btn btn-light">Return to device list</a>

<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content modal-success">
        <span class="close">&times;</span>
        <p>Operation successfully completed</p>
    </div>

</div>

<script>
    $("#createForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        let form = $(this);
        let url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                $('#createForm')[0].reset();
                modal();
            }
        });
    });

    changeTypeImg();
    $("#output_type_id").change(function(e) {
        changeTypeImg();
    });
</script>