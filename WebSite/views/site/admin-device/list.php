<?php require_once ROOT . '/views/layouts/header.php'; ?>
<div class="admin-house-list">
    <table id="devices" class="table table-striped">
        <thead>
        <tr>
            <?php foreach($columns as $column): ?>
                <th><?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?></th>
            <?php endforeach; ?>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($devices as $device): ?>
        <tr>
            <?php foreach($columns as $column): ?>
                <td><?php echo $device[$column['COLUMN_NAME']]; ?></td>
            <?php endforeach; ?>
            <td>
                <a href="/admin/device/edit/<?php echo $device['id']?>" class="btn btn-light">Edit</a>
                <a href="/admin/device/delete/<?php echo $device['id']?>" class="btn btn-danger delete-article" data-id="<?php echo $device['id']?>">Delete</a>

            </td>
        </tr>

        <?php endforeach; ?>
        </tbody>

    </table>

    <div class="create-link">
        <a href="/admin/device/create"><h3 class="btn btn-primary">Create Device</h3></a>
    </div>

</div>


