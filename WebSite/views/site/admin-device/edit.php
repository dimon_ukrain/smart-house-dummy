<?php

require_once ROOT . '/views/layouts/header.php'; ?>


<form method="post" id="createForm" action="/admin/roomDevice/edit/<?php echo $device['id']; ?>" class="create">
    <div class="create-block">
        <h1>Edit device</h1>
        <p>Please fill in this form to edit an <?php echo $device['name']; ?>.</p>
        <hr>

        <?php foreach($columns as $column): ?>
            <?php if($column['COLUMN_NAME'] == 'description'):?>
            <textarea name="device[<?php echo $column['COLUMN_NAME'];?>]"
                      placeholder="<?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?>"
                      cols="30" rows="10"><?php echo $device[$column['COLUMN_NAME']]; ?></textarea>

            <?php elseif($column['COLUMN_NAME'] == 'output_type_id'): ?>
                <div class="device-output-type">
                    <label for="device[<?php echo $column['COLUMN_NAME'];?>]">Output type: </label>
                    <select id="output_type_id" name="device[<?php echo $column['COLUMN_NAME'];?>]">
                        <?php foreach(Output_type::getTypes() as $type): ?>
                            <option value="<?php echo $type['id']?>"><?php echo $type['type'];?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <img src="../../../template/img/output-types/" id="output-type-img" alt="">

            <?php elseif($column['COLUMN_NAME'] != 'id'): ?>
                <input type="text"
                       name="device[<?php echo $column['COLUMN_NAME'];?>]"
                       placeholder="<?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?>"
                       value="<?php echo $device[$column['COLUMN_NAME']]; ?>"/>
            <?php endif;?>
        <?php endforeach; ?>

        <input type="submit" value="Send form"/>
    </div>
</form>

<a href="/admin/device/list" class="btn btn-light">Return to device list</a>

<script>
    changeTypeImg();
    $("#output_type_id").change(function(e) {
        changeTypeImg();
    });


    $("#createForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                location.replace("/admin/device/list");
            }
        });
    });
</script>