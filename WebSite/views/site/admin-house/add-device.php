<?php

require_once ROOT . '/views/layouts/header.php'; ?>


<form method="post" id="createForm" action="/admin/houseRoom/<?php echo $houseRoomId?>/add/device" class="create">
    <div class="create-block">
        <h1>Add device to room</h1>
        <p>Please fill in this form to add an device to room.</p>
        <hr>

        <div class="room-select">
            <label for="device_id">Device:</label>

            <select name="device_id">
                <?php foreach($devices as $device): ?>
                    <option value="<?php echo $device['id']?>"><?php echo $device['name']?></option>
                <?php endforeach; ?>
            </select>

        </div>
        <div class="room-select">
            Device Value: <input type="text" name="device_value">

        </div>

        <p><input type="submit" value="Send form"/></p>
    </div>
</form>

<div class="create-link">
    <a href="/admin/houseRoom/<?php echo $houseRoomId;?>/devicesList" class="btn btn-light">Return to device list</a>
</div>
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content modal-success">
        <span class="close">&times;</span>
        <p>Operation successfully completed</p>
    </div>

</div>

<script>
    $("#createForm").submit(function(e) {

        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),

            // serializes the form's elements.
            success: function(data)
            {
                $('#createForm')[0].reset();
                modal();
            }
        });


    });
</script>