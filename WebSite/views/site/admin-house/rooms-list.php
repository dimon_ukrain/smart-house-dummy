<?php require_once ROOT . '/views/layouts/header.php'; ?>

<div class="admin-house-list">
    <table id="houseRooms" class="table table-striped">
        <thead>
        <tr>
            <?php foreach($columns as $column): ?>
                <th><?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?></th>
            <?php endforeach; ?>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <? foreach($houseRooms as $houseRoom): ?>
            <tr>
                <?php foreach($columns as $column): ?>
                    <?php if($column['COLUMN_NAME'] == 'id'):?>
                        <?php foreach($rooms as $room): ?>
                            <?php if($houseRoom['room_id'] == $room['id']):?>
                                <td><?php echo $houseRoom['id']; ?></td>
                            <?php endif;?>
                        <?php endforeach;?>

                    <?php else: ?>
                        <?php foreach($rooms as $room): ?>
                            <?php if($houseRoom['room_id'] == $room['id']):?>
                                <td><?php echo $room[$column['COLUMN_NAME']]; ?></td>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                <?php endforeach;?>

                <td>
                    <a href="/house/<?php echo $houseRoom['house_id']; ?>/room/<?php echo $houseRoom['room_id']; ?>" class="btn btn-dark">Show</a>
                    <a href="/admin/houseRoom/delete/<?php echo $houseRoom['id']; ?>" class="btn btn-danger delete-article" data-id="<?php echo $houseRoom['id']; ?>">Delete</a>

                    <a href="/admin/houseRoom/<?php echo $houseRoom['id'];?>/devicesList" class="btn btn-green" data-id="<?php echo $houseRoom['id'] ?>">Devices List</a>
                </td>

            </tr>

        <?php endforeach; ?>
        </tbody>

    </table>

    <div class="create-link">
        <a href="/admin/house/<?php echo $house['id']?>/add/room" class="btn btn-green" data-id="<?php echo $house['id']?>">Add room</a>
    </div>
    <div class="create-link">
        <a href="/admin/house/list" class="btn btn-light">Return to houses list</a>
    </div>

</div>