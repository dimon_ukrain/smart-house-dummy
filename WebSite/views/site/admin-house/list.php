<?php require_once ROOT . '/views/layouts/header.php'; ?>
<div class="admin-house-list">
    <table id="houses" class="table table-striped">
        <thead>
        <tr>
            <?php foreach($columns as $column): ?>
                <th><?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?></th>
            <?php endforeach; ?>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($houses as $house): ?>
        <tr>
            <?php foreach($columns as $column): ?>
                <td><?php echo $house[$column['COLUMN_NAME']]; ?></td>
            <?php endforeach; ?>
            <td>
                <a href="/house/<?php echo $house['id']?>" class="btn btn-dark">Show</a>
                <a href="/admin/house/edit/<?php echo $house['id']?>" class="btn btn-light">Edit</a>
                <a href="/admin/house/delete/<?php echo $house['id']?>" class="btn btn-danger delete-article" data-id="<?php echo $house['id']?>">Delete</a>
                <a href="/admin/house/<?php echo $house['id']?>/roomsList" class="btn btn-green" data-id="<?php echo $house['id']?>">Rooms List</a>
            </td>
        </tr>

        <?php endforeach; ?>
        </tbody>

    </table>

    <div class="create-link">
        <a href="/admin/house/create"><h3 class="btn btn-primary">Create House</h3></a>
    </div>

</div>


