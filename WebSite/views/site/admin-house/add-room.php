<?php

require_once ROOT . '/views/layouts/header.php'; ?>


<form method="post" id="createForm" action="/admin/house/<?php echo $houseId?>/add/room" class="create">
    <div class="create-block">
        <h1>Add room to house</h1>
        <p>Please fill in this form to add an room to house.</p>
        <hr>

        <p>House: <?php echo $house['description']?></p>

        <div class="room-select">
            <label for="room_id">Room:</label>

            <select name="room_id">
                <?php foreach($rooms as $room): ?>
                    <option value="<?php echo $room['id']?>"><?php echo $room['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <p><input type="submit" value="Send form"/></p>
    </div>
</form>

<p><a href="/admin/house/<?php echo $houseId?>/roomsList" class="btn btn-light">Return to room list</a></p>

<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content modal-success">
        <span class="close">&times;</span>
        <p>Operation successfully completed</p>
    </div>

</div>

<script>
    $("#createForm").submit(function(e) {

        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),

            // serializes the form's elements.
            success: function(data)
            {
                $('#createForm')[0].reset();
                modal();
            }
        });


    });
</script>