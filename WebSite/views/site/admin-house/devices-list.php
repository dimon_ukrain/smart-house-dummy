<?php require_once ROOT . '/views/layouts/header.php'; ?>

<div class="admin-house-list">
    <table id="roomDevices" class="table table-striped">
        <thead>
        <tr>
            <?php foreach($roomDeviceColumns as $column): ?>
                <?php if($column['COLUMN_NAME'] == 'device_id'):?>
                    <th><?php echo "Device name";?></th>
                <?php else: ?>
                    <th><?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?></th>
                <?php endif; ?>
            <?php endforeach; ?>
            <th>Actions</th>

        </tr>
        </thead>
        <tbody>
        <? foreach($roomDevices as $roomDevice): ?>
            <tr>
                <?php foreach($roomDeviceColumns as $column): ?>
                    <?php if($column['COLUMN_NAME'] == 'id'):?>
                        <?php foreach($devices as $device): ?>
                            <?php if($roomDevice['device_id'] == $device['id']):?>
                                <td><?php echo $roomDevice['id']; ?></td>
                            <?php endif;?>
                        <?php endforeach;?>

                    <?php elseif($column['COLUMN_NAME'] == 'device_id'):?>
                        <?php foreach($devices as $device): ?>
                            <?php if($roomDevice['device_id'] == $device['id']):?>
                                <td><?php echo $device['name']; ?></td>
                            <?php endif;?>
                    <?php endforeach;?>

                    <?php else: ?>
                        <?php foreach($devices as $device): ?>
                            <?php if($roomDevice['device_id'] == $device['id']):?>
                                <td><?php echo $roomDevice[$column['COLUMN_NAME']]; ?></td>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                <?php endforeach;?>

                <td>
                    <a href="/admin/roomDevice/delete/<?php echo $roomDevice['id']; ?>" class="btn btn-danger delete-article" data-id="<?php echo $roomDevice['id']; ?>">Delete</a>
                    <a href="/admin/roomDevice/edit/<?php echo $roomDevice['id']?>" class="btn btn-light">Edit</a>
                </td>
             </tr>

        <?php endforeach; ?>
        </tbody>

    </table>

    <div class="create-link">
        <a href="/admin/houseRoom/<?php echo $houseRoom['id']; ?>/add/device" class="btn btn-green">Add Device</a>
    </div>

    <div class="create-link">
        <a href="/admin/house/<?php echo $houseRoom['room_id']; ?>/roomsList" class="btn btn-light">Return to rooms list</a>
    </div>

</div>