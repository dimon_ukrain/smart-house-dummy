<?php

require_once ROOT . '/views/layouts/header.php'; ?>


<form method="post" id="createForm" action="/admin/<?php echo $createdObject;?>/create" class="create">
    <div class="create-block">
        <h1>Create <?php echo ucfirst($createdObject);?></h1>
        <p>Please fill in this form to create an <?php echo $createdObject;?>.</p>
        <hr>

        <?php foreach($columns as $column): ?>
            <?php if($column['COLUMN_NAME'] != 'id'): ?>
                <input type="text" name="<?php echo $createdObject;?>[<?php echo $column['COLUMN_NAME'];?>]"
                       placeholder="<?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?>"/>
            <?php endif;?>
        <?php endforeach; ?>

        <input type="submit" value="Send form"/>
    </div>
</form>

<a href="/admin/<?php echo $createdObject ?>/list" class="btn btn-light">Return to <?php echo $createdObject ?> list</a>

<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content modal-success">
        <span class="close">&times;</span>
        <p>Operation successfully completed</p>
    </div>

</div>

<script>
    $("#createForm").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        let form = $(this);
        let url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                $('#createForm')[0].reset();
                modal();
            }
        });


    });
</script>