<?php require_once ROOT . '/views/layouts/header.php'; ?>

<div class="admin-arduino-list">

    <table id="arduinos" class="table table-striped">
        <thead>
            <tr>
                <?php foreach($columns as $column): ?>
                    <th><?php echo ucfirst(str_replace('_', " ", $column['COLUMN_NAME']));?></th>
                <?php endforeach; ?>
                <th>Actions</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach($arduinos as $arduino): ?>
            <tr>
                <?php foreach($columns as $column): ?>
                    <td><?php echo $arduino[$column['COLUMN_NAME']]; ?></td>
                <?php endforeach; ?>
                <td>
                    <a href="/admin/arduino/edit/<?php echo $arduino['id']?>" class="btn btn-light">Edit</a>
                    <a href="/admin/arduino/delete/<?php echo $arduino['id']?>" class="btn btn-danger delete-article" data-id="<?php echo $arduino['id']?>">Delete</a>

                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>

    </table>

    <div class="create-link">
        <a href="/admin/arduino/create"><h3 class="btn btn-primary">Create arduino</h3></a>
    </div>
    <div class="create-link">
        <a href="/admin" class="btn btn-light">Return to admin</a>
    </div>
</div>


