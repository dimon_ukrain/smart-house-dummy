<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/fontawesome.min.css">
    <link rel="stylesheet" type="text/css" href="/template/css/header.css">
    <link rel="stylesheet" type="text/css" href="/template/css/account.css">
    <link rel="stylesheet" type="text/css" href="/template/css/form-create.css">
    <link rel="stylesheet" type="text/css" href="/template/css/login-form.css">
    <link rel="stylesheet" type="text/css" href="/template/css/room-fix.css">
    <link rel="stylesheet" type="text/css" href="/template/css/houses.css">
    <link rel="stylesheet" type="text/css" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/register-form.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>

<body>

<header>
    <div class="logo">
        <a href="/"><h2>Smart house</h2></a>
    </div>

    <div class="nav-bar">
        <div class="dropdown">
            <?php if(isset($_SESSION['user']['username'])):?>
                <button class="dropbtn"><?php echo $_SESSION['user']['username']; ?></button>
            <?php endif;?>
            <div class="dropdown-content">
                <a href="/account">Account</a>
                <a href="/logout">Logout</a>
            </div>
        </div>
    </div>
</header>

<script src="/template/js/control_types.js"></script>
<script src="/template/js/main.js"></script>
